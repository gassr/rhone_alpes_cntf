
.. index::
   pair: Gilets Jaunes; TAS-RA
   pair: Gilets Jaunes; TAS-RA

.. _il_est_temps:

============================================================
Il est temps que les riches paient !
============================================================

.. seealso::

   - https://www.cnt-tas.org/2018/12/12/il-est-temps-que-les-riches-paient-greve-14-decembre-2018/


Après trois semaines de mouvement social dit des *gilets jaunes* le président
Macron a fini par s’exprimer.

A en croire certains journalistes il aurait opéré *un virage social*.

Quand on y regarde plus près, les mesures annoncées ressemblent plus à une vaste
opération d’enfumage qu’à une réponse aux besoins exprimés.

Macron avait clairement annoncé dès la départ que ces mesures ne coûteraient
rien aux employeurs : il a tenu promesse !

Pour ne reprendre ici que l’annonce emblématique d’une augmentation de 100
euros du SMIC, il ne s’agit aucunement d’une augmentation du taux horaire
du SMIC. Par un tour de passe-passe Macron ne fait qu’ajouter l’augmentation
due à la revalorisation obligatoire chaque année et le montant de la prime
d’activité financée sur les impôts.

S’agissant de la prime exceptionnelle de fin d’année que Macron a gentiment
demandé  aux entreprises *qui le peuvent*, le président MEDEF a tout de suite
déclaré qu’il ne fallait pas compter dessus.

Tout est à l’avenant...

Seule la suppression de la hausse de la CSG pour les petites retraites, a
constitué un petit recul.

Revenons donc à l’essentiel,pourquoi est-il difficile, y compris pour ceux qui
travaillent, de finir le mois ?
Parce que les salaires sont trop bas.

Une part de plus en plus grande de la richesse produite par les travailleurs
est captée par le capital. Le problème n’est pas entant que telle niveau des
impôts ou des cotisations, mais le fait que les riches en paient peu ou pas en
particulier grâce à l’optimisation ou l’évasion fiscale.

**La politique de classe** du gouvernement n’a fait qu’accentuer cette
tendance: suppression de l’ISF, suppression de l’exit tax, flat tax qui
plafonne l’impôt sur les revenus du capital, maintien du CICE, etc.

Rien, absolument rien dans les mesures annoncées par Macron ne vient régler
ce problème de fond.

Il faut que les riches rendent l’argent qu’ils volent aux travailleurs !

Aussi les réponses apportées à l’insuffisance des salaires réels par rapport
au coût de la vie,sont des réponses qui ne remettent aucunement en cause la
politique du gouvernement et qui coûteront cher aux travailleurs, aux chômeurs,
aux retraités.

L’amélioration immédiate du pouvoir d’achat, telle que la propose Macron,
sera payée par les salariés eux-mêmes, soit par les impôts, soit **par la
perte du salarié socialisé inhérent à la baisse des cotisations sociales**.

**Ne l’oublions pas! Les cotisations sociales, ce sont les soins médicaux,
les allocations familiales, les prestations chômage, les retraites...**

**Les cotisations sociales sont la part de nos salaires consacrée à la solidarité,
nous permettant de sécuriser collectivement nos conditions d’existence**.

La politique du gouvernement, qui ne se démarque des précédentes que par la
radicalité de la casse sociale, n’est qu’une arrogante politique de classe
qui détruit nos conquêtes sociales pour servir les intérêts des plus riches.

Au **mépris des riches et des puissants**, à leur **violence sociale et policière**,
il est nécessaire d’opposer une résistance sans faille, en construisant le
rapport de force nécessaire à notre victoire à tous, salariés, chômeurs,
fonctionnaires.

C’est pourquoi nous appelons à la grève le 14 décembre 2018 sur
les revendications suivantes :

Dans l’immédiat :

- augmentation du SMIC et de tous les salaires ;
- indexations des salaires sur les prix ;
- augmentation du point d’indice ;
- abrogation de la CSG, CRDS et de la TVA, et leur compensation par
  le retour au salaire socialisé et l’augmentation des cotisations patronales;
- déplafonnement des cotisations sociales pour les plus hauts salaires ;
- augmentation des effectifs des services chargés de lutter contre la fraude
  sociale et fiscale ;
- maintien du système de retraite par répartition et retour de la retraite à 60
  ans à taux plein ;
- réouverture des services publics de proximité et l’arrêt des suppressions
  de postes dans les services publics, le remplacement de tous les départs à
  la retraite par l’embauche sous statut ;
- blocage des loyers d’habitation.

A moyen terme :

- salaire horaire identique pour toutes et tous, public comme privé ;
- retour à une gestion du système de sécurité sociale par les travailleurs
  eux-mêmes, sans patron et sans État.
- Financement exclusif par les cotisations sociales et non pas par les impôts
  destinés à compenser les exonérations offertes aux entreprises ;
- abolition de l’héritage au-dessus d’un certain montant (on ne devient pas
  riche en travaillant mais en héritant ou en faisant travailler les autres).
