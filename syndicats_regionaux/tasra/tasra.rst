
.. index::
   pair: syndicat régional; TASRA
   pair: Courriel; direccte-rhona-ut69.cnt@direccte.gouv.fr


.. _stas_ra:
.. _tasra:

============================================================
Syndicat Travail Affaires Sociales Rhône-Alpes (TASRA)
============================================================


- https://www.cnt-tas.org
- http://www.cnt-f.org/spip.php?article1721

Adresse
=======


:Adresse courriel: direccte-rhona-ut69.cnt@direccte.gouv.fr


Contact CNT Travail et Affaire Sociale Rhône-Alpes
--------------------------------------------------


:Kévin GOUTELLE:  n° 06 75 71 34 91
:Email:  rhona-ut74.cnt@direccte.gouv.fr

Communiqués/tracts
====================

.. toctree::
   :maxdepth: 4

   2018/2018
   communiques/index


Motions
========

.. toctree::
   :maxdepth: 4

   motions_congres/motions_congres
