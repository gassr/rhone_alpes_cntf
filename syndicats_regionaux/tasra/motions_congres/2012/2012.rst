



.. _stas_ra_motions_2012:

=========================================================
Motions, Contre-motions amendements congres STAS-RA 2012
=========================================================


Amendements
===========

- :ref:`amendement_motion_17_2012_stas_ra`
- :ref:`amendement_motion_30_2012_stas_ra`
