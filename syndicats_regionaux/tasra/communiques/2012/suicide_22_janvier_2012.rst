
.. index::
   pair: Harcèlement; Suicide


.. _suicide_inspecteur_du_travail_2012:

===============================================================
Nouveau suicide d’un inspecteur du travail
===============================================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article1827

C’est avec consternation et colère que nous avons appris le suicide
le 18 janvier 2012 d’un de nos collègues, Romain Lecoustre, inspecteur du
travail à Lille.

Ce nouveau suicide vient illustrer de façon dramatique la dégradation de nos
conditions de travail et la responsabilité des différents niveaux d’une
hiérarchie zélée.

Romain Lecoustre avait déjà effectué une première tentative de suicide de
juillet 2011 face à la surcharge de travail et aux pressions de la hiérarchie
locale.
Il avait lui-même vainement tenté de faire reconnaître cet acte comme accident
de service.

Après avoir fait part d’une compassion de rigueur, les différents échelons de
la hiérarchie, jusqu’aux responsables nationaux, ne manqueront pas de se
dédouaner de leurs responsabilités pour faire du cas de Romain un cas particulier.

Pourtant pour nous, comme pour tous ses camarades, il n’y a de doute sur la
nature professionnelle de son mal-être, ni sur les causes de son suicide :
c’est bien le travail, ses conditions d’exercice et l’environnement
hiérarchique qui sont ici clairement responsables de la dégradation profonde
de son état de santé, qui l’a conduit à la mort.

Pression sur les chiffres ; fonctionnement des services à flux tendu ; déni de
la souffrance et dévalorisation des agents : la destruction de notre
travail finit par détruire les agents eux-mêmes et conduit parfois à des actes
désespérés.
