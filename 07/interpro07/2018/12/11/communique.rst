

.. index::
   pair: Gilets Jaunes ; CNT 07


.. _gilets_cnt_07_2018_12_11:

===============================================================
Position du syndicat CNT interpro 07 sur les *Gilets jaunes*
===============================================================

Maintenant 4 semaines que, comme partout en France, les *Gilets jaunes* occupent
les ronds-points dans la région d'Aubenas.

Si, au départ, nous étions réticents, compte tenu des propos racistes et
tendance extrême-droite de certains, ainsi que de leur défiance envers les
syndicats, la position de certains d'entre nous a évolué.

Le 1er décembre 2018 nous avons participé sous nos couleurs à la manifestation
organisée par la CGT.
Cette manifestation a défilé derrière des *Gilets jaunes*, une partie d'entre
eux restant à l'occupation des ronds-points.

Notre AG du dimanche 9 décembre 2018 a confirmé notre volonté d'être davantage
présents sur les ronds-points.
Sans faire forcément de la propagande pour notre syndicat, il nous paraît
important de tenir un discours **syndicaliste, de classe**, pour ne pas laisser
tout le champ libre aux discours nauséabonds qui circulent au quotidien parmi
ces gens qui veulent enfin lutter.

Lors de nos échanges, surtout après l'intervention de Maqueron, nous insistons
sur :

- la défense des cotisations sociales (supprimer les cotisations pour soi-disant
  augmenter le pouvoir d'achat comme il le propose n'est qu'un leurre, car si le
  salaire net augmente, le **salaire socialisé, lui, recule!**),
- la nécessité de grève générale, à notre avis seule possibilité de gagner, et
  plus efficace que le R.I.C. (Referendum d'Initiative Citoyenne), régulièrement
  évoqué sur les ronds-points comme LA solution,
- les idées habituellement portées par notre courant syndical : représentants
  mandatés et révocables, anti-capitalisme, et autres revendications immédiates
  concernant les salaires, indemnités chômage, tous minima sociaux et présence
  des services publics partout pour tous.

Coupés depuis longtemps de ce prolétariat (dont nous ne pouvions que constater
l'absence lors des précédents combats), il nous paraît indispensable
d'entretenir la discussion, pour tenter d'éviter le basculement massif de
cette population, comme en Hongrie, en Italie, au Brésil... et surtout pour
vaincre !

Rédigé en réunion syndicale du 11 décembre 2018,
Syndicat CNT interpro de l'Ardèche
