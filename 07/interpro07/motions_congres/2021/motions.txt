Motions de stratégie et orientations

Contre Motion N° 24 :

Argumentaire :

Dans  notre   société,   et  particulièrement  dans le   milieu   professionnel,  la  règle   appliquée  dans  les
affaires de violences sexistes ou sexuelles est trop souvent en défaveur de la victime, avec son
éloignement,   sous   couvert   de   protection,   et   le   maintien,   au   moins   temporaire,   de   son   potentiel
agresseur. La CNT ne saurait cautionner un fonctionnement similaire.

La CNT n’accepte pas les agresseur.euse.s sexuel.le.s  en son sein et doit se doter d’outils pour
traiter de l’efficience de cette volonté et protéger effectivement la ou les victimes.

C’est un fait que certaines instances de la société sont imprégnées de sexisme. Mais nombreuses
sont les organisations militantes qui mènent des actions, objet d’une réflexion féministe.

Afin que la prise en charge de la victime soit la plus efficace possible, il ne semble pas opportun que
la CNT cherche à se substituer aux associations, organisations et institutions spécialisées dans l’aide
et   la   prise   en   charge   de   la   ou   des   victimes.   Ces   organisations   ont   les   compétences   et   la
connaissance du tissu pluridisciplinaire que ces prises en charges requièrent.

C’est pourquoi , il nous parait plus judicieux de proposer une procédure applicable pour faire face à
ces situations et permettre aux syndicats concernés de pouvoir continuer à fonctionner en laissant à
une potentielle victime syndiquée d’ exercer son activité syndicale.

Pour nous, l‘autonomie des syndicats est le pacte confédéral qui nous unit à la CNT. Nous refusons
que cette autonomie soit remise en question pour quelque raison que ce soit. Pour autant, il est
important que la CNT se dote de ressources pour accompagner les syndicats désireux de ne pas
rester seuls dans ces situations.

Il   semble   que   la   mise   en   place   d’une   procédure,   applicable   dans   l’urgence,   peut   permettre   aux
syndicats concernés d’avoir le temps de personnaliser la conduite qu’ils souhaitent mettre en place.

Proposition de contre motion 24 :

 Titre : Gestion interne des violences sexuelles, sexistes et patriarcales

Toute personne reconnue coupable de viol/violences patriarcales n’a pas sa place à la CNT.

Toute  personne  syndiquée  dépositaire  de l’information d’une agression sexiste  ou sexuelle
commise au sein de la CNT se doit de faire remonter cette information au sein de son syndicat
et/ou du syndicat du potentiel agresseur sans pour autant nommer la victime si tel est le
choix de cette dernière.

Sachant que la présomption d’innocence reste de mise mais qu’elle ne prévaut pas sur la
mise en sécurité de la victime, l’accusé.e est suspendu.e temporairement par son syndicat. Il
doit être clair que cette suspension ne préjuge pas de sa culpabilité.

Cette suspension provisoire de l'accusé.e doit être mise à profit par le syndicat de l’accusé.e
pour :

1. Si la victime  se manifeste auprès du syndicat, recueillir ses souhaits sur la manière
dont elle désire être soutenue par le syndicat. Quoi qu’il en soit, lui apporter autant
d’informations   que   possibles   sur   les   accompagnements   et   soutiens   existants
(exemples : Planning Familial, Centre d’Information Droits Femmes Familles, diverses
associations locales…)

2. Le syndicat ne peut pas faire l’économie de  temps d’échange entre  ses adhérent.e.s
sur la conduite à tenir à moyen et long termes (exclusion du mailing liste, exclusion du
syndicat,... ) en fonction des suites données par la victime. Pour ce faire, le syndicat ne
doit   pas   hésiter   à   mobiliser   toutes   les   ressources   internes   à   la   CNT   (commission
antisexiste ..., UD, UR…)

3. Le syndicat ne doit pas se substituer à un tribunal et les réunions d’échanges ont pour
but de permettre la continuité de la vie syndicale dans le respect de la victime et ne doit
pas se transformer en procès d’intention à l’égard de l’accusé.e.

4. La CNT ne possède pas, actuellement, en son sein les compétences pour instruire une

enquête et établir un jugement.

Cette   procédure   vise,   dans   l’urgence,   à   protéger   la   victime   et   permettre   au   syndicat   de
prendre le temps d’élaborer une réflexion nécessaire à la mise en œuvre de mesures plus
pérennes.

Cette procédure a pour vocation d’assurer la continuité de la vie syndicale dans le respect
des valeurs et des luttes anarcho-syndicalistes  mais aussi antisexistes, anti-patriarcales et
antifascistes chères à la CNT, en gardant à l’esprit la nécessité de protéger la victime.
