
.. index::
   pair: Syndicat; Interpro01


.. _interpro_01_pub:

=======================================
Syndicat CNT Interpro01
=======================================

.. seealso:: :ref:`ud01_publique`



::

    c/o 44 rue Burdeau BP 26000
    69218 Lyon
    Mail : interpro01@cnt-f.org
