
.. index::
   pair: syndicat; educ69


.. _educ69:

=======================================
Syndicat CNT educ69 (Lyon)
=======================================

.. seealso:: :ref:`ud69_publique`



Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/index
