
.. index::
   pair: syndicat; SUB69
   ! SUB69


.. _sub_69_pub:

=======================================
Syndicat CNT SUB TPE BAMC 69 (Lyon)
=======================================

.. seealso::

   - :ref:`ud69_publique`
   - http://www.cnt-f.org/sub69


.. figure:: logo_sub69.png
   :align: center

   *Logo SUB69*

Adresse SUB TPE BAMC 69
=======================

Syndicat Unique du Bâtiment, des Travaux Publics, de l'Equipement, du
Bois, de l’Ameublement et des Matériaux de constructions  du Rhône


::

   CNT SUB 69
   44, rue Burdeau
   69001 Lyon
   Tél. 07 86 94 69 21


:Adresse courriel: sub69@cnt-f.org


Permanences
===========

Pour nous rencontrer rien de plus simple : la permanence du 1er lundi de
chaque mois et sur rendez-vous au 07 86 94 69 21.


Activités
=========

.. toctree::
   :maxdepth: 4

   activites/index
