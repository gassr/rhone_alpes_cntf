

.. _activites_SUB69_2011:

=======================================
Activites SUB69 2011
=======================================


Nouveau `site internet`_, travail sur les sans-papiers (quelle aide post
régularisation à apporter, logement...).

S’investissent également sur la redynamisation de l’:ref:`UD <ud69_publique>`.


.. _`site internet`:  http://www.cnt-f.org/sub69/
