
.. index::
   pair: syndicat; Sanso69


.. _sanso_69_pub:

=======================================
Syndicat CNT sanso69 (Lyon)
=======================================

.. seealso:: :ref:`ud69_publique`


::

   CNT interco 69
   44, rue Burdeau
   69001 Lyon

Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
