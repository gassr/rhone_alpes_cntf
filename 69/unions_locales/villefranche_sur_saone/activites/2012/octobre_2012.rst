
.. index::
   pair: Solidarité; Pérou

.. _activites_villefranche_69_octobre_2012:

====================================================
Soutien au peuple de Cajamarca (Pérou) Octobre 2012
====================================================

.. seealso::

   - http://www.cnt-f.org/video/videos/44-international/415-soutien-au-peuple-de-cajamarca-perou
   - http://cnt-f.org/video/images/stories/images/autre/Cajamarca.pptx
   - http://www.packupload.com/
   - http://www.packupload.com/IY57VUT4VG9
   - http://uslperu.blogspot.fr/


Introduction
============

L’UL CNT de Villefranche Beaujolais a organisé le 12 octobre 2012 au local
alternatif « La ruche des citoyens » une soirée en soutien au peuple de
Cajamarca (Pérou).

Y a été diffusé le film documentaire mis en ligne ci-dessous.

Ce film a été réalisé par des militants de l'UL CNT Villefranche.
Les différentes scènes ont été prises ou récupérées par Sam, militant du
syndicat CNT santé-social 69, présent à Cajamarca depuis février 2012.

Pour en savoir plus, voir le texte et les liens figurant sous la fenêtre vidéo.
Voici le lien :
http://www.cnt-f.org/video/videos/44-international/415-soutien-au-peuple-de-cajamarca-perou


Résumé de la situation à Cajamarca
==================================

compte-rendu de la soirée du 12 octobre et autres informations :

Depuis septembre 2011, la population de Cajamarca se bat contre l’extension de
l'exploitation d’une mine d’or qui menace l'une des plus grandes réserves
d'eau potable de la région. Cette mobilisation pacifique s’est transformée
durant l’été 2012 par une lutte meurtrière à cause de la  répression violente
de la part de la police d’État soutenue par des milices de la société
exploitant la mine d’or.

Les militants locaux nous ont interpellés pour relayer l’information sur les
nombreuses victimes autour de cette lutte : 52 morts à travers tout le pays et
des centaines de blessés ; sans parler des risques à venir de pollution de
l'eau par les métaux lourds issus de l'extraction de cet or (cyanure et mercure).

Aucune info internationale n'a été transmise par les médias sur ces violences
commises. Cela n'est dénoncé par personne, même à l’intérieur du Pérou où ces
informations ne sont pas connues.

Les habitants de Lima ne sont pas informés.

Ils ne voient que des images de violence à la télévision qui laissent planer
l'ombre d'une guérilla et du terrorisme. Ils ne se doutent pas de la lutte
louable du peuple. Il faut que ça se sache ! Les classes moyennes suivent le
discours du gouvernement et pensent que les bénéfices de cette exploitation
minière permettront de développer la région, sans se poser plus de questions.

Boycott également pour annoncer l’organisation de la soirée du 12 octobre à
Villefranche ! En effet, l'information n'a été relayée que par une radio
locale (Radio Calade), la presse écrite locale refusant de publier le
programme de la soirée. Nous sommes donc allés au contact de la population
et avons diffusé l'information dans l'un des quartiers populaires de la ville
ainsi que dans les librairies, restaurants, etc.

Lors de la soirée, nous étions une trentaine de personnes présentes.

Nous avions réalisé une expo-photos témoignant de la mobilisation du peuple
et des nouvelles atrocités subies par le peuple et les militants cautionnées
par l’État et les milices locales. La soirée s’est poursuivie par la projection
d’un documentaire poignant réalisé a Cajamarca et par la présentation plus
globale de la situation au Pérou à l'aide d'une présentation « powerpoint »
où nous avons abordé également les actions mises en place par nos camarades
de l’USL (Union Socialista Libertaria).

La soirée s’est prolongée  avec de nombreux échanges dans la salle, ponctués
par une p’tite bouffe à prix libre en soutien à une fondation de volontaires
qui soignent sur place les nombreux blessés et viennent en aide aux familles
des victimes.

La présentation « powerpoint », l’expo-photos et le film mis en ligne
ci-dessus dans une version compressée (mais disponible aussi dans son format
d'origine, en DVD ou en libre téléchargement) sont à disposition des syndicats
et associations qui voudraient relayer l’info.

Nous réorganiserons certainement en décembre une autre soirée de soutien et
d'information avec l’intervention de Sam, notre camarade de la CNT présent à
Cajamarca depuis février 2012.

::

	UL CNT de Villefranche Beaujolais, 19 rue de Prony,
	69400 Villefranche
	Tél : 06 61 74 43 82
