
.. index::
   pair: Union Locales; Villefranche-sur-Saône
   pair: Villefranche ; sur-Saône


.. _ul_villefranche_69:

=======================================
Union locale Villefranche-sur-Saône
=======================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Villefranche-sur-Sa%C3%B4ne


Adresse
=======

::

    UL CNT de Villefranche Beaujolais
    19 rue de Prony,
    69400 Villefranche
    Tél : 06 61 74 43 82
    interpro.villefranche@cnt-f.org


Courriel: interpro.villefranche@cnt-f.org
==========================================

:couriel:  interpro.villefranche@cnt-f.org

Présentation wikipedia
======================

Villefranche-sur-Saône est une commune française, située dans le département du
Rhône et la région Rhône-Alpes.

Située dans le sud-est de la France, la ville se situe à 35 km de Lyon et 434
km de Paris.

Elle est la capitale du Beaujolais.

Ses habitants sont appelés les Caladois.

Fondée par les sires de Beaujeu en 1140 dans le but de protéger le Beaujolais
des archevêques de Lyon et particulièrement de la forteresse d'Anse, la ville
va connaitre une période de prospérité industrielle avec notamment des
innovations comme la Blédine.

Seule sous-préfecture du département du Rhône, la population totale de la
ville était de 34 159 habitants tandis que la population de l'arrondissement
était de 172 826 habitants pour l'année 2008.

Historique CNT
==============

Depuis mars 2012 une Union Locale est crée à  Villefranche-sur saône dans
le  département du Rhône.

A ce jour, 4 adhérents de santé-social 69 sont présents dans cette UL
ainsi  que 2 adhérents Interco .


Activités Villefranche-sur-Saône
=================================

.. toctree::
   :maxdepth: 3

   activites/index
