
.. index::
   pair: Tracts; UD69 (2014)


.. _tract_1er_mai_2014_ud69:

=======================================
Tracts 1er mai 2014 (UD69)
=======================================

.. seealso::

   - http://www.scribd.com/doc/221406298/Tract-1er-Mai-2014-CNT-Lyon


:download:`Tract du 1er mai 2014 UD69 <tract_1er_mai_2014.pdf>`
