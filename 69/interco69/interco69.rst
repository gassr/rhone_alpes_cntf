.. index::
   pair: Syndicat; Interco69

.. _interco_69_pub:
.. _interco69:

=======================================
Syndicat **CNT interco69** (Lyon)
=======================================

.. seealso::

   - :ref:`ud69_publique`

::

   CNT interco 69
   44, rue Burdeau
   69001 Lyon


.. toctree::
   :maxdepth: 4

   motions_congres/motions_congres
