
.. index::
   pair: UD69; Tefal
   pair: Soutien ; Laura Pfeiffer

.. _tefal_69_laura_2015:

======================================
Luttes UD69 (Rhône) contre Tefal 2015
======================================


.. seealso::

    - http://www.cnt-tas.org/2015/12/04/affaire-tefal-inspectrice-travail-condamnation-justice-pfeiffer/


Un salarié et une Inspectrice du travail attaqués pour avoir dénoncé les
pressions exercées par Tefal!

Déclaration intersyndicale des organisations du Ministère du travail et
des organisations interprofessionnelles départementales de Haute-Savoie,
le 4 décembre 2015

A la suite de l’audience du 16 octobre 2015 ayant vu comparaître, à la
suite d’une plainte de l’entreprise TEFAL, une inspectrice du travail
(Laura PFEIFFER) et un ancien salarié lanceur d’alerte, notamment pour
vol et recel de documents, la décision a été rendue : nous apprenons que
l’inspectrice du travail a été condamnée, sur les deux chefs d’accusation,
à  3500 euros d’amende avec sursis.

De plus l’ex-salarié lanceur d’alerte poursuivi pour vol a été condamné
à 3500 euros d’amende avec sursis.
Ces condamnations sont inscrites au casier judiciaire.

L’ensemble des condamnations est bien au delà des réquisitions déjà
fortes du procureur.

Nous sommes indignés et stupéfaits de ce verdict. Si nous attendons de
connaître la motivation retenue, nous voulons dès aujourd’hui dire que
ce jugement n’est pas audible par les salariés et par l’ensemble de la
profession des contrôleurs et inspecteurs du travail : il n’est pas
possible de condamner une inspectrice du travail pour n’avoir fait
que son travail.
Il n’est pas possible de condamner un lanceur d’alerte pour avoir joué
ce rôle essentiel d’aiguillon.

Le déroulé de cette procédure et du procès lui-même, constitue une sévère
mise en garde à destination des salariés et des agents qui ont pour
mission de contrôler les entreprises.

En effet, l’audience du 16 octobre 2015 a vu la mise en cause d’un
lanceur d’alerte pour avoir mis en œuvre ce droit ainsi que celle
d’une inspectrice du travail pour avoir exercé ses missions de base
puis dénoncé les pressions visant à faire obstacle à son travail.

Cette audience à sens unique, éprouvante et humiliante pour les intéressés,
aura malheureusement été l’occasion d’envoyer un signal fort à tous les
travailleurs et travailleuses :

L’inspection du travail dérange et il convient de la mettre au pas !

Selon le procureur, il faut y faire le ménage. Il en est de même pour
les syndicats, voire le droit du travail comme ont pu dire ou laisser
entendre l’avocat de TEFAL dans sa plaidoirie, le procureur dans son
réquisitoire ou encore la juge dans ses questions.

Ainsi que l’a relevé l’avocat de notre collègue, Maître Leclerc, lors
de sa plaidoirie le 16 octobre 2015,  « Certes nous ne sommes pas dans
un monde de bisounours, c’est d’ailleurs pour ça qu’il y a des inspecteurs
du travail », avant de poursuivre « Le problème qui est posé ici
c’est la place des syndicats dans notre société ».

Laura Pfeiffer et le lanceur d’alerte de TEFAL ont subi un procès honteux.

Honteux car il est le symbole de la collusion entre le patronat et les
hauts cadres de l’Etat.

Honteux car cela fait maintenant 3 ans que l’administration du Travail
se complait dans un silence écœurant que l’ensemble des agents de
l’inspection du travail, aux côtés de Laura Pfeiffer, ne peuvent comprendre.

Honteux car notre mission, protéger les salariés de l’arbitraire, a été
piétinée.

Nous n’acceptons pas ce jugement et poursuivrons le combat en appel pour
la relaxe, nous poursuivrons aussi nos combats aux côtés des agents et
des salariés.
Nous étions en colère le 5 juin 2015, nous l’étions le 16 octobre 2015,
et croyez bien que nous le sommes encore aujourd’hui car le mépris
décomplexé des différents protagonistes pour l’inspection du travail
et la condition des travailleurs est insupportable.

Nous demandons :

- la relaxe pour l’ex salarié lanceur d’alerte et l’inspectrice du travail
- la condamnation publique par la Ministre  de l’entreprise TEFAL pour
  les pressions inacceptables qu’elle a mis en œuvre  à l’encontre de
  l’inspectrice du travail
- la poursuite devant la justice des procédures initiées par notre collègue
- la reconnaissance de tous les  accidents de service de notre collègue
- le soutien de la Ministre aux missions de l’inspection du travail,
  à ses agents et tout particulièrement à Laura Pfeiffer.

Nous appelons l’ensemble des collègues et des salariés à organiser des
assemblées générales afin de mobiliser sur ces revendications.

--

    CNT – CGT – FSU – Solidaires – FO


Pétition de soutien à Laura et Christophe:

- http://plaintetefalsolidarite.wesign.it/fr


Pour information, le compte twitter du lanceur d'alerte Christophe:

- https://twitter.com/Tof73savoie
- https://twitter.com/Tof73savoie/status/653812068974505984

Librinfo 74

- http://www.librinfo74.fr/2015/12/laura-pfeiffer-condamnee/
