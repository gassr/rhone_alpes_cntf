
.. index::
   pair: labellisation; interco74 (26 novembre 2011)

.. _activites_interco74_2011:

=======================================
Activites interco74 2011
=======================================


Labellisation du syndicat interco74 par :ref:`L'Union régionale Rhône Alpes <union_regionale_ra>`
le 26 novembre 2011
