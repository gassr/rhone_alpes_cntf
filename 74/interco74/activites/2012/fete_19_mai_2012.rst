
.. index::
   pair: Activités; interco74
   pair: Haute-Savoie; Fếte de la CNT 74
   pair: Fếte de la CNT 74; 2012


.. _fete_interco74_2012:

=======================================
Fête de la CNT 74 (19 mai 2012)
=======================================


.. seealso::

   - http://www.fsd74.org/spip.php?article3592
   - http://alter-local.blogspot.fr/
   - http://cntinterco74.noblogs.org/



Présentation
============

::

	samedi 19 mai
	15h
	site de l’alterlocal à Cran

Un nouveau syndicat en Haute Savoie !

Autogestion, Démocratie, Décroissance : Construisons dès aujourd’hui la société
que nous voulons pour demain !

Projection du film "un autre futur" de Richard Prost, ou comment l’Espagne a
su construire un syndicalisme de transformation sociale.

Suivi d’un débat, d’un repas coopératif.

Concerts et DJ jusqu’au bout de la nuit.

Plus d’infos sur la CNT74 :

- http://cntinterco74.noblogs.org/
- Cnt-interco-74@cnt-f.org

Alterlocal
==========

L'adresse de la page d'accueil de la liste est : https://lists.riseup.net/www/info/alterlocal
Informations générales au sujet des listes de diffusions:

- https://lists.riseup.net/www/help/introduction
