
.. index::
   pair: syndicat; interco74
   pair: interco; 74


.. _interco_74_pub:

=======================================
Syndicat CNT interco74 (Annecy)
=======================================

.. seealso::

   - :ref:`ud74_publique`
   - http://cntinterco74.noblogs.org/


:Adresse courriel: cnt-interco-74@cnt-f.org


Adresse
========

::

    CNT interco 74
    Alterlocal
    3 chemin des Grèves
    74960 Cran Gevrier


Activites interco74
===================

.. toctree::
   :maxdepth: 4

   activites/index


Site Web CNT74
===================

.. toctree::
   :maxdepth: 4

   site_web/index
