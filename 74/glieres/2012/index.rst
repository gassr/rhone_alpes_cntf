

.. index::
   pair: Haute-Savoie; Glières 2012
   pair: Citoyens; Résistants


===================================================================================
Quand un air mauvais souffle sur la France, tous à Thorens-Glières les 26 et 27 mai
===================================================================================

.. seealso::

   - http://www.fsd74.org/spip.php?article3591
   - http://www.citoyens-resistants.fr/
   - http://www.citoyens-resistants.fr/spip.php?article232

Le résultat du premier tour des élections présidentielles interpelle notre
association. Un air mauvais souffle sur la France. Un air soufflé par la fille
de son père et lamentablement repris par celui qui devrait être le représentant
de la république française et de tous les français.

Quelle honte de le voir gesticuler en divisant les ouvriers entre les "vrais"
et les "faux" travailleurs, en distinguant constamment sur l’origine ou la
religion (rappelons son propos : « musulmans, en tout cas d’apparence, puisque
l’un était catholique. »)

Quelle colère de voir les privilégiés et les financiers toujours plus favorisés
alors que ceux qui devraient être protégés sont fragilisés voire exclus.

L’air de ces dernières semaines n’est pas sans rappeler celui qui soufflait sur
notre pays dans les années trente ! Cet air-là est toujours celui qui a mené
au pire. Il est temps de réagir.


Lors de nos rassemblements à Thorens et aux Glières les 26 mai et 27 mai
prochains, une large place sera donnée à cette question de la montée de
l’extrême droite notamment avec la venue de Gustav Tamas (philosophe et penseur
politique) et Mihály Csakó (sociologue) opposants au gouvernement de droite
extrême en Hongrie. Nous n’en resterons pas simplement au constat.
Nous envisagerons l’avenir.


Tout au long du samedi, François Ruffin animera un forum intitulé « Que faire ? »
où se succèderont de nombreux d’intervenants qui nous donneront des pistes de
réflexion pour maintenant et notre futur.

L’avenir se jouera aussi au cinéma  de Thorens Glières où des films porteurs
d’espoirs seront projetés avec débats complémentaires.

L’avenir sera débattu avec des intervenants tel que Eric Toussaint ainsi qu’un
intervenant grec sur la dette, Gérard Filoche, les Fralibs  et Bernard Friot
sur le monde du travail.

Paul Ariès posera la question  environnementale, et bien d’autres intervenants
lors de forums largement ouverts à l’intervention de chacune et chacun.

Une journée qui se conclura autour de Daniel Mermet.

Même si nous changeons de président de la République, ne rêvons pas, le
nouveau aura bien besoin d’un  coup de pouce des citoyens  (entre les élections
présidentielles et les  législatives) pour lui rappeler où sont les valeurs qui
fondent notre république et ses valeurs.

Le dimanche, nous entendrons encore de belles paroles sur le Plateau des Glières.
Résistants d’hier et d’aujourd’hui se partageront la tribune pour nous rappeler
de ne jamais baisser la tête ni se résigner. Charles Palant, ancien résistant
déporté, fondateur du Mrap, et les autres seront là pour nous le rappeler, pour
remettre de la dignité dans le discours ambiant.

Si cet air mauvais souffle aujourd’hui, comme l’a souvent répété Raymond Aubrac
à qui nous rendrons un hommage tout particulier, l’optimisme permet de déplacer
des montagnes. Avec force et détermination, eux ont su inventer un monde
meilleur en rédigeant le programme du CNR alors que tout semblait perdu.
A nous d’apporter notre pierre à l’édifice.

Tous ensemble les 26 et 27 mai

Communiqué de l’association « Citoyens Résistants d’Hier et d’Aujourd’hui »
