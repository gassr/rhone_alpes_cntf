.. index::
   pair: syndicats; CNT Rhône-Alpes
   pair: Union Régionale ; Rhône-Alpes
   pair: Courriel ;  ur-ra@cnt-f.org
   ! URRA
   ! Rhône-Alpes


.. figure:: images/bandoconf.jpg
   :align: center


.. figure:: images/repartition_egalitaire_des_richesses.jpg
   :align: center

.. _urra:
.. _union_regionale_ra:
.. _syndicats_rhone_alpes:

=======================================
CNT **Union Régionale Rhône Alpes**
=======================================

- http://cnt-f.org
- :ref:`congres_cnt_f`
- :ref:`glossaire_cnt`
- :ref:`glossaire_juridique`
- http://www.cnt-f.org/spip.php?article19
- https://fr.wikipedia.org/wiki/Rh%C3%B4ne-Alpes
- http://www.cnt-f.org/annuaire-des-syndicats-et-unions-locales-de-la-cnt.html#listeSyndicats
- https://gitlab.com/cnt-f/rhone_alpes
- https://gitlab.com/cnt-f/rhone_alpes/-/boards


.. figure:: rhone_alpes.png
   :align: center


.. contents::
   :local:

:code région: 82


Description
=============

Le Rhône-Alpes (arpitan: Rôno-Arpes; occitan : Ròse-Aups) est une région
française qui regroupe huit départements :

- Ain (01)
- Ardèche (07)
- Drôme (26)
- Isère (38)
- Loire (42)
- Rhône (69)
- Savoie (73)
- Haute-Savoie (74)


Son chef-lieu est Lyon, qui est aussi sa plus grande ville.
Avec 44000 km2, elle est la deuxième région de France métropolitaine en
superficie (après Midi-Pyrénées), en économie et en population
avec 6 117 229 habitants (après Île-de-France).

Elle est aussi classée 6e région européenne.

Elle est limitrophe des régions françaises:

- Provence-Alpes-Côte d'Azur,
- Languedoc-Roussillon,
- Auvergne,
- Bourgogne
- Franche-Comté,

des régions italiennes de Val d'Aoste et Piémont, ainsi que des cantons
suisses de Vaud, du Valais et de Genève.

Adresse
=======

::

    UR CNT Rhône-Alpes
    44 rue Burdeau
    69001 Lyon


:Adresse courriel: ur-ra@cnt-f.org

Les Syndicats régionaux de la région CNT Rhône Alpes
=======================================================

.. toctree::
   :maxdepth: 4

   syndicats_regionaux/syndicats_regionaux


Les Unions départementales de la région CNT Rhône Alpes
=======================================================


.. toctree::
   :maxdepth: 4

   01/01
   07/07
   26/26
   38/38
   42/42
   69/69
   73/73
   74/74


.. toctree::
   :maxdepth: 3

   meta/meta
