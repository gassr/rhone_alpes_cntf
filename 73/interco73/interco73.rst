
.. index::
   pair: syndicat; Interco73
   pair: interco; Ugine
   ! Ugine

.. _interco_73_pub:
.. _interco73:

================================================================================================================
Syndicat interco73 (interco73, Ugine)
================================================================================================================

.. seealso:: :ref:`ud73_publique`


::

    CNT Interco 73
    Maison des Syndicats
    73400 UGINE
    Mail : cnt-interco-savoie@cnt-f.org




Motions interco73
==================

.. toctree::
   :maxdepth: 3

   motions_congres/motions_congres
