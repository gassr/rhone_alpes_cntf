
.. index::
   pair: Syndicats; STP26


.. _stp26:

==================================================================
Syndicat C.N.T. des Travailleurs et Précaires de la Drôme (STP26)
==================================================================




Label confédéral
================

::

    Sujet :  Circulaire n°11 : cahier des mandatéEs, orga 33ème Congrès, Solidarité Fouad ...
    Date :  Sun, 09 Nov 2014 17:31:30 +0100


Concernant les labellisations, en lien direct avec les discussions du
dernier CCN en ayant validé le principe, les syndicats CNT des travailleurs
et précaires précaires de la Drôme (STP 26) et CNT des Employés et travailleurs
précaires du Commerce de l’Industrie et des Services des Pyrénées Orientales
(ETPICS 66) ont été labellisés respectivement les 13 Octobre 2014 et
1er septembre 2014.
Ces syndicats sont attendus au Congrès Confédérale de Décembre 2014


Statuts du syndicat
===================

:download:`Télécharger les statuts du syndicat STP26 <statuts_stp26.pdf>`
