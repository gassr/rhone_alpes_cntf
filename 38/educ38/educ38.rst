
.. index::
   pair: syndicat; educ38


.. _educ_38_pub:

=======================================
Syndicat Educ38
=======================================

- :ref:`ud38_publique`


Adresse
=======

::

   CNT Educ38
   BP385, 38015
   Grenoble Cedex 1


Adresse courriel
================

:Adresse courriel:  educ.38@cnt-f.org


Motions congrès
==================

.. toctree::
   :maxdepth: 3

   motions_congres/motions_congres
