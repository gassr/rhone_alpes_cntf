.. index::
   ! meta-infos


.. _rh_meta_infos:

=====================
Meta infos
=====================


.. seealso::

   - https://cnt-f.gitlab.io/meta/


Gitlab project
================

.. seealso::

   - https://gitlab.com/cnt-f/rhone_alpes

Issues
--------

.. seealso::

   - https://gitlab.com/cnt-f/rhone_alpes/-/boards


Pipelines
------------

.. seealso::

   - https://gitlab.com/cnt-f/rhone_alpes/-/pipelines


pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
