
.. index::
   pair: syndicat; educ42


.. _educ_42_pub:

=======================================
Syndicat Educ42
=======================================

.. seealso::

   - :ref:`ud42_publique`
   - http://www.cnt-f.org/cnt42/rubrique15.html



Activités
=========

.. toctree::
   :maxdepth: 4

   activites/activites
