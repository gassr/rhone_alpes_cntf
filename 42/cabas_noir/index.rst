
.. index::
   pair: UD42; cabas noir
   pair: coopérative; cabas noir

.. _cabas_noir_42:

=======================
Coopérative Cabas Noir
=======================

.. seealso:: http://www.cnt-f.org/cnt42/rubrique3.html

Le Cabas Noir s’installe dans ces nouveau locaux au 91 rue Antoine
Durafour à Saint-Etienne à partir du vendredi 14 octobre 2011.
