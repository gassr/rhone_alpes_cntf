
.. index::
   pair: syndicat; Santé social 42


.. _sanso_42_pub:

=======================================
Syndicat Santé social 42
=======================================

.. seealso::

   - :ref:`ud42_publique`
   - http://www.cnt-f.org/cnt42/rubrique16.html



Activités
=========

.. toctree::
   :maxdepth: 4

   activites/index
